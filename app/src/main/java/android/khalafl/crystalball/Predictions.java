package android.khalafl.crystalball;

import android.content.Context;
import android.media.MediaPlayer;

public class Predictions {
    private static Predictions predictions;
    private String[] answers;

    private Predictions(){
        answers = new String[]{
                "Your wishes will come true.",
                "Your wishes will never come true.",
                "No one likes sour cream",
                "Why do you ask a ball of crystal"
        };

    }

    public static Predictions get() {
        if(predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }
    public String getPrediction(){
        return answers[3];
    }
}
