package android.khalafl.crystalball;

//import android.animation.ObjectAnimator;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.util.FloatMath;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import static android.media.MediaPlayer.create;


public class CrystalBall extends AppCompatActivity {

    private TextView answerText;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private float acceleration;
    private float currentAcceleration;
    private float previousAcceleration;

    private final SensorEventListener sensorListener= new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event){
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            previousAcceleration = currentAcceleration;
            currentAcceleration = (float) Math.sqrt(x * x + y * y + z * z);
            float delta = currentAcceleration - previousAcceleration;
            acceleration = acceleration *0.9f +delta;//a bucket that leaks water at a constant rate, so the equation does not go to zero

            if(acceleration > 15){
                //MediaPlayer mediaPlayer =
                //create(getApplicationContext(), R.raw.crystal_ball);
                //mediaPlayer.start();
                Toast toast = Toast.makeText(getApplication(), "device has shaken", Toast.LENGTH_SHORT);
                toast.show();
                answerText.setText(Predictions.get().getPrediction());
                TextView textView = (TextView) findViewById(R.id.answerText);
                textView.setVisibility(View.VISIBLE);
//                ObjectAnimator anim = ObjectAnimator.ofFloat(answerText, "alpha", 0f, 1f);
//                anim.setDuration(1000);
//                anim.start();
            }

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

    };


    @Override//@Override means that we can over ride manually what the application does within the start up
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crystal_ball);

        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);//different types of sensors, accelerometer is one of them

        acceleration = 0.0f;
        currentAcceleration = SensorManager.GRAVITY_EARTH;//you can do gravity fof anywhere
        previousAcceleration = SensorManager.GRAVITY_EARTH;

        answerText = (TextView) findViewById(R.id.answerText); //text view is a cast, changes what is being called
//        answerText.setText(Predictions.get().getPrediction());
    }
    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(sensorListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(sensorListener);

    }


}
